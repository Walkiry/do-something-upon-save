# do-something-upon-save

A simple script to execute command upon modification (saves) of a file

Based on the dev of Eric Zhiqiang Ma from github:
(https://raw.githubusercontent.com/zma/usefulscripts/master/script/event-driver-cmd/do-after-change.sh)

# Usage

./do-something-upon-save "cmd_to_execute" *.c *.cpp CMakeLists.txt



